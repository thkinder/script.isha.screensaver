#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#     Copyright (C) 2013 Team-XBMC
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
import xbmc
import xbmcgui
import xbmcaddon
import time
import json

screensaver_gui = None

"""
Activate screen saver after a video is over, this will make sure we do not see
the menu after video has stopped.
"""
class AutoBlackScreen(xbmc.Player):
    def __init__(self):
        xbmc.Player.__init__(self)
        self.newGui = None

    def setup(self, gui, winId):
        self.newGui = gui
        self.winId = winId

    def onPlayBackEnded(self):

        cmd =  '{"jsonrpc":"2.0","method":"Settings.GetSettingValue","params":{"setting":"screensaver.mode"}, "id":25}'
        ret = xbmc.executeJSONRPC(cmd)
        decode = json.loads(ret)
        slidemode = decode['result']['value']

        if self.newGui:
            if xbmcgui.getCurrentWindowDialogId() != self.winId and slidemode.lower() != 'none':    #int(playLength) <= 1:
                self.newGui.show()
                xbmc.log('+++++++++++++++ Playback ended screen saver', level=xbmc.LOGNOTICE)



class ScreenSaver:

    def __risingEdge(self, current, last):
        if self.__edgeDetect(current, last) == "rising":
            return True

        return False

    def __fallingEdge(self, current, last):
        if self.__edgeDetect(current, last) == "falling":
            return True

        return False

    def __edgeDetect(self, current, last):
        if current == False and last == False:#no change in signal
            return "low"
        elif current == True and last == False:#rising edge
            return "rising"
        elif current == True and last == True:#np change
            return "high"
        else:#falling edge
            return "falling"

    def Main(self):
        addon = xbmcaddon.Addon('script.isha.screensaver')
        path = addon.getAddonInfo('path')
        homewin = xbmcgui.Window(10000)

        idleTime = int(addon.getSetting('ss_duration'))
        gui = xbmcgui.WindowXMLDialog('script-fastss-main.xml', path, 'default',)

        isPlayingLast = False
        isPlaying = False
        sigCurrent = False
        sigLast = False

        """
        This is a little workarround to get the windows ID of the screen saver
        window. With that we can check if the window is currently open or not
        I was not able to find another way to get the window ID of the screen
        saver gui nor was I able to find a way to check if windows is open
        without having it's ID. When kodi is started this will show the black
        screen for a short period so screen flashes once, not perfect but works
        IsDialogRunnign() method does not seem to exist here, or not working...
        """
        gui.show()
        winId = xbmcgui.getCurrentWindowDialogId()
        gui.close()

        autoBlack = AutoBlackScreen()
        autoBlack.setup(gui, winId)

        #main service loop
        while not xbmc.abortRequested: #True:

            globalIdleTime =  xbmc.getGlobalIdleTime()
            invoke_screensaver = homewin.getProperty('invoke_screensaver')
            isPlaying = xbmc.Player().isPlaying()

            sigCurrent = invoke_screensaver == "1" or (globalIdleTime > idleTime and (not isPlaying))

            startSaverREdge = self.__risingEdge(sigCurrent, sigLast)
            stopSaverFEdge = self.__fallingEdge(sigCurrent, sigLast)
            sigLast = sigCurrent

            if startSaverREdge: # activate screen saver
                #xbmc.log('------------- newSaver activated (idle time = {t})'.format(t=globalIdleTime), level=xbmc.LOGNOTICE)
                if xbmcgui.getCurrentWindowDialogId() != winId:
                    gui.show()
		    homewin.setProperty("invoke_screensaver", "0")

            elif stopSaverFEdge:
                #xbmc.log('------------- newSaver closed', level=xbmc.LOGNOTICE)
                gui.close()

            xbmc.sleep(500)


if (__name__ == "__main__"):
    ScreenSaver().Main()
